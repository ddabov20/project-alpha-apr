David Dabov

## Functionality

- Users can create an account through a signup form
- Users can log in with a created account through a login form
- Users can create a project that they can assign to any account
- Once this project is created it will be displayed as a list with the name in one column and the number of tasks assigned to that project in the other column
- Users can click on the name of that project to go to a new page which list all of the tasks assigned to that project
- Users can create tasks with a start and end date and they can assign to any project and to any account
- Users will have a navbar that will have links to all the pages as well as a logout link to log the user out
- When the user is logged out they can only go to the signup page and the login page

## Project Initialization

To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Clone the repository down to your local machine
2. CD into the new project directory
3. Create a virtual Environment (python -m venv .venv)
4. Activate the virtual Environement
  - source ./.venv/bin/activate  # macOS
  - ./.venv/Scripts/Activate.ps1 # Windows
4. Run python -m pip install --upgrade pip
5. Run pip install -r requirements.txt
6. Run python manage.py runserver
7. Go to localhost:8000 in browser
